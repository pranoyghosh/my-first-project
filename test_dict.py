#dictionary for employee at Persistent . Initially fields are name, job title, blood group. 
#then add two new fields like grade, job family
#I am using fixture and markers
import pytest
import pytest
dict={"Name":"Pranoy Ghosh","Job Title":"Software Engineering","Blood Group":"O+"}
@pytest.fixture
def input_value():
#we add values from user
   val= input("\n Grade? ")
   dict['Grade']=val
   val= input("\n Job family? ")
   dict['Job family']=val
   return dict
@pytest.mark.checkvalue
def test_grade(input_value):
    val1=float(input_value['Grade'])
    val2=input_value['Job family']
    assert (val1==3.1 and val2=='Engineering Development')
