# a small program where I check whether an input number is divisible by 2, 3, 5 and 7 . 
# I have also used fixtures and markers
import pytest
@pytest.fixture
def input_value():
   number = int(input("\n number? "))
   return number
@pytest.mark.isdiv
def test_iseven(input_value):
    assert input_value % 2 == 0
@pytest.mark.isdiv
def test_isdivby3(input_value):
    assert input_value % 3 == 0
@pytest.mark.isdiv
def test_isdivby5(input_value):
    assert input_value % 5 == 0
@pytest.mark.isdiv
def test_isdivby7(input_value):
    assert input_value % 7 == 0
